package com.t1.yd.tm.dto.response.user;

import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.Nullable;

public class UserGetProfileResponse extends AbstractUserResponse {

    public UserGetProfileResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

    public UserGetProfileResponse(@Nullable User user) {
        super(user);
    }

    public UserGetProfileResponse() {
        super();
    }

}
