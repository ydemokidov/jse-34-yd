package com.t1.yd.tm.model;

import com.t1.yd.tm.enumerated.Role;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractUserOwnedEntity {

    @NotNull
    private Date date = new Date();
    @Nullable
    private Role role = null;

    public Session(@NotNull String userId) {
        super(userId);
    }

}
