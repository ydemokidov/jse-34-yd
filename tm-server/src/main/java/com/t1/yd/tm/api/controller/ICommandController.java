package com.t1.yd.tm.api.controller;

public interface ICommandController {

    void showAbout();

    void showVersion();

    void showHelp();

    void showInfo();

    void showCommands();

    void showArguments();

    void showArgumentError();

    void showCommandError();

    void showWelcome();

}
